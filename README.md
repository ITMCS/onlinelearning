

# Backend server

## All of the bellow technologies are used --

### Spring Boot 2.3.1.RELEASE

### Hibernate & JPA

### Maven

### Mysql 



# Build & run  Maven Spring Boot Applications 

git clone project from server git server


Project name : onlinelearning

goto where pom.xml is keep

# install Spring boot project

`mvn install`

# run Spring boot project

`mvn spring-boot:run`

# run spring boot project as jar file 

`java -jar target/onlinelearning.jar`