package com.online.learning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.learning.entity.CourseTransaction;
import com.online.learning.response.CourseTransactionBean;
import com.online.learning.service.api.CourseTransactionService;

@RestController
@RequestMapping("coursetransactions")
public class CourseTransactionController {

	@Autowired
	CourseTransactionService courseTransactionService;
	
	@PostMapping
	public CourseTransaction addCourseTransaction(@RequestBody CourseTransaction courseTransaction) {
	    return courseTransactionService.save(courseTransaction);
	}
	
	@GetMapping
	public List<CourseTransaction> fetchAllCourseTransaction() {
	    return courseTransactionService.fetchAll();
	}
	
	@GetMapping("details/{courseid}/{countryid}")
	public CourseTransactionBean getCourseTransaction(@PathVariable("courseid") long courseId,
			@PathVariable("countryid") long countryId) {
	    return courseTransactionService.getCourseDetailsByCountryCourseId(courseId, countryId);
	}
}
