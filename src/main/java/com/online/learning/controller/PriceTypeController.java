package com.online.learning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.learning.entity.PriceType;
import com.online.learning.service.api.PriceTypeService;

@RestController
@RequestMapping("priceTypes")
public class PriceTypeController {

	@Autowired
	PriceTypeService priceTypeService;
	
	@PostMapping
	public PriceType addPriceType(@RequestBody PriceType priceType) {
	    return priceTypeService.save(priceType);
	}
	
	@GetMapping
	public List<PriceType> fetchAllPriceType() {
	    return priceTypeService.fetchAll();
	}
	
	@GetMapping("{id}")
	public PriceType getPriceType(@PathVariable Long id) {
	    return priceTypeService.getById(id);
	}
}
