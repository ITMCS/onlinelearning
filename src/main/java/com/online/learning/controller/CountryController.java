package com.online.learning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.learning.entity.Country;
import com.online.learning.service.api.CountryService;

@RestController
@RequestMapping("countries")
public class CountryController {

	@Autowired
	CountryService countryService;
	
	@PostMapping
	public Country addCountry(@RequestBody Country country) {
	    return countryService.save(country);
	}
	
	@GetMapping
	public List<Country> fetchAllCountry() {
	    return countryService.fetchAll();
	}
	
	@GetMapping("{id}")
	public Country getCountry(@PathVariable Long id) {
	    return countryService.getById(id);
	}
}
