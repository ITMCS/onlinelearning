package com.online.learning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.learning.entity.Course;
import com.online.learning.service.api.CourseService;

@RestController
@RequestMapping("courses")
public class CourseController {

	@Autowired
	CourseService courseService;
	
	@PostMapping
	public Course addCourse(@RequestBody Course course) {
	    return courseService.save(course);
	}
	
	@GetMapping
	public List<Course> fetchAllCourse() {
	    return courseService.fetchAll();
	}
	
	@GetMapping("{id}")
	public Course getCourse(@PathVariable Long id) {
	    return courseService.getById(id);
	}
	
}
