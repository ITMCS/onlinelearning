package com.online.learning.service.api;

import java.util.List;

import com.online.learning.entity.CourseTransaction;
import com.online.learning.response.CourseTransactionBean;

public interface CourseTransactionService {

	CourseTransaction save(CourseTransaction courseTransaction);
	
	CourseTransactionBean getCourseDetailsByCountryCourseId(long courseId,long countryId);
	
	List<CourseTransaction> fetchAll();
}
