package com.online.learning.service.api;

import java.util.List;

import com.online.learning.entity.PriceType;

public interface PriceTypeService {

	PriceType save(PriceType priceType);
	
	PriceType getById(long priceTypeId);
	
	List<PriceType> fetchAll();
}
