package com.online.learning.service.api;

import java.util.List;

import com.online.learning.entity.Course;

public interface CourseService {

	Course save(Course course);
	
	Course getById(long courseId);
	
	List<Course> fetchAll();

}
