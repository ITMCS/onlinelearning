package com.online.learning.service.api;

import java.util.List;

import com.online.learning.entity.Country;

public interface CountryService {

	Country save(Country country);
	
	Country getById(long countryId);
	
	List<Country> fetchAll();
}
