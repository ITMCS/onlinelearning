package com.online.learning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.learning.entity.PriceType;
import com.online.learning.exception.RecordNotFoundException;
import com.online.learning.repository.PriceTypeRepository;
import com.online.learning.service.api.PriceTypeService;

@Service
public class PriceTypeServiceImpl implements PriceTypeService{

	@Autowired
	PriceTypeRepository priceTypeRepository;
	
	@Override
	public PriceType save(PriceType priceType) {
		return priceTypeRepository.save(priceType);
	}

	@Override
	public PriceType getById(long priceTypeId) {
		return priceTypeRepository.findById(priceTypeId).orElseThrow(() -> new RecordNotFoundException("No priceType exist with "+priceTypeId+" id"));
	}

	@Override
	public List<PriceType> fetchAll() {
		return priceTypeRepository.findAll();
	}

}
