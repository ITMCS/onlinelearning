package com.online.learning.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.learning.entity.CourseTransaction;
import com.online.learning.exception.RecordNotFoundException;
import com.online.learning.repository.CourseTransactionRepository;
import com.online.learning.response.CourseTransactionBean;
import com.online.learning.service.api.CourseTransactionService;

@Service
public class CourseTransactionServiceImpl implements CourseTransactionService{

	@Autowired
	CourseTransactionRepository courseTransactionRepository;
	
	@Override
	public CourseTransaction save(CourseTransaction courseTransaction) {
		return courseTransactionRepository.save(courseTransaction);
	}

	@Override
	public List<CourseTransaction> fetchAll() {
		return courseTransactionRepository.findAll();
	}

	@Override
	public CourseTransactionBean getCourseDetailsByCountryCourseId(long courseId,long countryId) {
		List<CourseTransaction> courseTransactions = courseTransactionRepository.
				findByCourseIdAndCountryIdAndIsActive(courseId,countryId,true);
		if(!courseTransactions.isEmpty()) {
			return calculateCourseList(courseTransactions);
		}else {
			throw new RecordNotFoundException("No course exist with Course: "+courseId+" & Country: "+countryId+" id");
		}
	}
	
	private CourseTransactionBean calculateCourseList(List<CourseTransaction> courseTransactions) {
		CourseTransactionBean courseTransactionBean = new CourseTransactionBean();
		long totalamount = 0;
		Map<String, Long> pricetypes = new HashMap<>();
		for(CourseTransaction courseTransaction: courseTransactions) {
			totalamount = totalamount + courseTransaction.getAmount();
			pricetypes.put(courseTransaction.getPriceType().getName(), courseTransaction.getAmount());
			courseTransactionBean.setCourse(courseTransaction.getCourse().getName());
			courseTransactionBean.setCountry(courseTransaction.getCountry().getName());
		}
		courseTransactionBean.setAppliedprices(pricetypes);
		courseTransactionBean.setTotalamount(totalamount);
		return courseTransactionBean;
	}

}
