package com.online.learning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.learning.entity.Course;
import com.online.learning.exception.RecordNotFoundException;
import com.online.learning.repository.CourseRepository;
import com.online.learning.repository.CourseTransactionRepository;
import com.online.learning.service.api.CourseService;

@Service
public class CourseServiceImpl implements CourseService{

	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	CourseTransactionRepository courseTransactionRepository;
	
	@Override
	public Course save(Course course) {
		return courseRepository.save(course);
	}

	@Override
	public Course getById(long courseId) {
		return courseRepository.findById(courseId).orElseThrow(() -> new RecordNotFoundException("No course exist with "+courseId+" id"));
	}

	@Override
	public List<Course> fetchAll() {
		return courseRepository.findAll();
	}

}
