package com.online.learning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.learning.entity.Country;
import com.online.learning.exception.RecordNotFoundException;
import com.online.learning.repository.CountryRepository;
import com.online.learning.service.api.CountryService;

@Service
public class CountryServiceImpl implements CountryService{

	@Autowired
	CountryRepository countryRepository;
	
	@Override
	public Country save(Country country) {
		return countryRepository.save(country);
	}

	@Override
	public Country getById(long countryId) {
		return countryRepository.findById(countryId).orElseThrow(() -> new RecordNotFoundException("No country exist with "+countryId+" id"));
	}

	@Override
	public List<Country> fetchAll() {
		return countryRepository.findAll();
	}

}
