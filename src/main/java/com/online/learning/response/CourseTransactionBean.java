package com.online.learning.response;

import java.util.Map;

public class CourseTransactionBean {

	private String course;
	private String country;
	private long totalamount;
	private Map<String,Long> appliedprices;
	
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public long getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(long totalamount) {
		this.totalamount = totalamount;
	}
	public Map<String, Long> getAppliedprices() {
		return appliedprices;
	}
	public void setAppliedprices(Map<String, Long> appliedprices) {
		this.appliedprices = appliedprices;
	}
	
}
