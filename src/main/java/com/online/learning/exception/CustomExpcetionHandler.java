package com.online.learning.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;




@ControllerAdvice
public class CustomExpcetionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity<Object> handleRecordNotFoundException(RecordNotFoundException ex, WebRequest request) {
        String details = ex.getLocalizedMessage();
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("error", 404);
        responseMap.put("message", "Record Not Found");
        responseMap.put("details", details);
        return new ResponseEntity<Object>(responseMap, HttpStatus.NOT_FOUND);
    }
    
}