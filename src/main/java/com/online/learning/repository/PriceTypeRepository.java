package com.online.learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.learning.entity.PriceType;

@Repository
public interface PriceTypeRepository extends JpaRepository<PriceType, Long>{

}
