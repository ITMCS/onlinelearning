package com.online.learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.online.learning.entity.CourseTransaction;

@Repository
public interface CourseTransactionRepository extends JpaRepository<CourseTransaction, Long>{

	List<CourseTransaction> findByCourseIdAndCountryIdAndIsActive(long courseId,long countryId,boolean status);

}
