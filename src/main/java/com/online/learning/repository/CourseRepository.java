package com.online.learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.learning.entity.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>{

}
